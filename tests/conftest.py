from typing import Any
from typing import Dict

import pytest
from requests_mock import Mocker


@pytest.fixture
def foods(requests_mock: Mocker) -> Dict[str, Any]:
    foods = {
        'foods': [
             {'fdcId': 1455174,
              'description': 'EGG',
              'lowercaseDescription': 'egg',
              'dataType': 'Branded',
              'gtinUpc': '768395547691',
              'publishedDate': '2021-02-26',
              'brandOwner': 'Ross Acquisition Inc.',
              'ingredients': 'SUGAR, CORN SYRUP, MODIFIED CORNSTARCH, LACTIC ACID, MALIC ACID, SODIUM LACTATE, CITRIC ACID, GUM ARABIC, CARNAUBA WAX, WHITE MINERAL OIL, BEESWAX, NATURAL AND ARTIFICIAL FLAVORS, SHELLAC, SODIUM CITRATE, YELLOW 5, RED 40, YELLOW 6, BLUE 1.',
              'allHighlightFields': '',
              'score': 913.7018,
              'foodNutrients': [
                  {'nutrientId': 1003,
                   'nutrientName': 'Protein',
                   'nutrientNumber': '203',
                   'unitName': 'G',
                   'derivationCode': 'LCCS',
                   'derivationDescription': 'Calculated from value per serving size measure',
                   'value': 0.0},
                  {'nutrientId': 1004,
                   'nutrientName': 'Total lipid (fat)',
                   'nutrientNumber': '204',
                   'unitName': 'G',
                   'derivationCode': 'LCCD',
                   'derivationDescription': 'Calculated from a daily value percentage per serving size measure',
                   'value': 0.0},
                  {'nutrientId': 1005,
                   'nutrientName': 'Carbohydrate, by difference',
                   'nutrientNumber': '205',
                   'unitName': 'G',
                   'derivationCode': 'LCCS',
                   'derivationDescription': 'Calculated from value per serving size measure',
                   'value': 90.0},
                  {'nutrientId': 1008,
                   'nutrientName': 'Energy',
                   'nutrientNumber': '208',
                   'unitName': 'KCAL',
                   'derivationCode': 'LCCS',
                   'derivationDescription': 'Calculated from value per serving size measure',
                   'value': 367},
                  {'nutrientId': 2000,
                   'nutrientName': 'Sugars, total including NLEA',
                   'nutrientNumber': '269',
                   'unitName': 'G',
                   'derivationCode': 'LCCS',
                   'derivationDescription': 'Calculated from value per serving size measure',
                   'value': 63.3},
                  {'nutrientId': 1093,
                   'nutrientName': 'Sodium, Na',
                   'nutrientNumber': '307',
                   'unitName': 'MG',
                   'derivationCode': 'LCCS',
                   'derivationDescription': 'Calculated from value per serving size measure',
                   'value': 150},
                  {'nutrientId': 1235,
                   'nutrientName': 'Sugars, added',
                   'nutrientNumber': '539',
                   'unitName': 'G',
                   'derivationCode': 'LCCS',
                   'derivationDescription': 'Calculated from value per serving size measure',
                   'value': 63.3}]},
        ]}

    requests_mock.get('https://api.nal.usda.gov/fdc/v1/foods/search', json=foods)

    return foods
