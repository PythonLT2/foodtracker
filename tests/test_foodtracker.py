import pytest

from foodtracker.services import json_product_list, create_product_list


@pytest.mark.django_db(True)
def test_index(django_app):
    resp = django_app.get('')
    assert resp.status_code == 200


def test_json_product_list(foods: dict):
    assert json_product_list('egg') == foods


def test_create_product_list(foods: dict):
    product_list = [{
        'Product name': 'EGG',
        'Protein': 0.0,
        'Carbs': 90.0,
        'Energy': 367,
        'Sugars': 63.3,
        'Sodium': 150,
        'Trans fat': '0'
    }]
    assert create_product_list(foods) == product_list
