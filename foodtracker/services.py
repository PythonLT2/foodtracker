from django.conf import settings

import requests
from django.db.models import Sum, FloatField

from foodtracker.models import Product


def json_product_list(product: str) -> dict:
    """This function takes provided product and outputs
    full list of matching products in the FoodData Central database."""
    url = 'https://api.nal.usda.gov/fdc/v1/foods/search'
    params = {
        'api_key': str(settings.API_KEY),
        'query': product
    }
    resp = requests.get(url, params=params)
    resp.raise_for_status()
    return resp.json()


def create_product_list(product_dict: dict) -> list:
    """This function filters search results and
     forms a list of products with nutrition list"""
    product_list = []
    product_description = {}
    for i in range(0, len(product_dict['foods'])):
        product_description['Product name'] = \
            product_dict['foods'][i]['description']
        trans_fat = 0
        trans_fat_poly = 0
        trans_fat_mono = 0
        for j in range(0, len(product_dict['foods'][i]['foodNutrients'])):
            # Searching value for energy
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1008:
                product_description['Energy'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for protein
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1003:
                product_description['Protein'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for sugars
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 2000:
                product_description['Sugars'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for carbs
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1005:
                product_description['Carbs'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for cholesterol
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1253:
                product_description['Cholesterol'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for trans fat
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1257:
                trans_fat = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for trans_fat_poly
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1293:
                trans_fat_poly = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for trans_fat_mono
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1292:
                trans_fat_mono = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for sat_fat
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1258:
                product_description['Sat fat'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for Sodium
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1093:
                product_description['Sodium'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for Fiber
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1079:
                product_description['Fiber'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for Vitamin C
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1162:
                product_description['Vitamin C'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for Vitamin D
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1110:
                product_description['Vitamin D'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for Calcium
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1087:
                product_description['Calcium'] = product_dict['foods'][i]['foodNutrients'][j]['value']
            # Searching value for Iron
            if product_dict['foods'][i]['foodNutrients'][j]['nutrientId'] == 1089:
                product_description['Iron'] = product_dict['foods'][i]['foodNutrients'][j]['value']
        trans_fat_combo = trans_fat + trans_fat_mono + trans_fat_poly
        product_description['Trans fat'] = str(round(trans_fat_combo, 2))
        product_list.append(dict(product_description))
    return product_list


def save_to_database(product_dict: dict):
    product = Product(
        product_name=product_dict.get('Product name', 0),
        energy=product_dict.get('Energy', 0),
        protein=product_dict.get('Protein', 0),
        carbs=product_dict.get('Carbs', 0),
        fiber=product_dict.get('Fiber', 0),
        calcium=product_dict.get('Calcium', 0),
        sodium=product_dict.get('Sodium', 0),
        iron=product_dict.get('Iron', 0),
        vitamin_c=product_dict.get('Vitamin C', 0),
        vitamin_d=product_dict.get('Vitamin D', 0),
        cholesterol=product_dict.get('Cholesterol', 0),
        sat_fat=product_dict.get('Sat fat', 0),
        trans_fat=product_dict.get('Trans fat', 0)
    )
    product.save()


def calculate_total():
    total_values = []
    columns = [field.name for field in Product._meta.get_fields() if field.name not in ['product_name', 'id']]
    for column in columns:
        sum_value = Product.objects.aggregate(total=Sum(column, output_field=FloatField()))
        if sum_value.get('total') is None:
            break
        else:
            total_values.append(round(sum_value.get('total'), 2))
    return total_values
