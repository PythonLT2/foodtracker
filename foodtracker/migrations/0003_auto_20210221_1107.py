# Generated by Django 3.1.7 on 2021-02-21 09:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('foodtracker', '0002_auto_20210221_1105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='receipt',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='foodtracker.receipt'),
        ),
    ]
