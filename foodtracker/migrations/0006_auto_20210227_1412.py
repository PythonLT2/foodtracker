# Generated by Django 3.1.7 on 2021-02-27 12:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('foodtracker', '0005_auto_20210227_1143'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='vitamin_a',
            new_name='vitamin_c',
        ),
        migrations.AddField(
            model_name='product',
            name='energy',
            field=models.DecimalField(decimal_places=3, default=0, max_digits=20),
        ),
    ]
