import json
import re

from django.http.request import HttpRequest
from django.shortcuts import render, redirect

from foodtracker.models import Product
from foodtracker.services import create_product_list, json_product_list, save_to_database, calculate_total


def index(request: HttpRequest):
    return render(request, 'index.html')


def food(request: HttpRequest):
    if 'product' in request.GET:
        product = request.GET.get('product')
        if product and re.match(r"^[\w ]+$", product):
            response = create_product_list(json_product_list(product))
        else:
            response = []
    else:
        response = []

    if len(response) > 0:
        columns = list(response[0].keys())
    else:
        columns = []

    return render(request, 'index.html', {
        'columns': columns,
        'response': response,
        'products': Product.objects.all(),
        'total_values': calculate_total()
    })


def get_data(request: HttpRequest):
    add = request.POST.get("add", "")
    add = add.replace("\'", "\"")
    product = json.loads(add)
    save_to_database(product)
    return redirect(food)


def remove_data(request: HttpRequest):
    remove = request.POST.get("remove", "")
    productId = int(remove)
    Product.objects.filter(id=productId).delete()
    return redirect(food)


def delete_all(request: HttpRequest):
    Product.objects.all().delete()
    return redirect(food)
