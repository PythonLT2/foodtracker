from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=200, blank=True)
    energy = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    protein = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    carbs = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    sugars = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    fiber = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    calcium = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    iron = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    sodium = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    vitamin_c = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    vitamin_d = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    cholesterol = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    sat_fat = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    trans_fat = models.DecimalField(max_digits=20, decimal_places=2, default=0)

    def __str__(self):
        return self.product_name
